
class NodoArbol {
 int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol(int d, String a){
        this.dato = d;
        this.nombre = a;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
        public String toString(){
            return nombre + "El dato es: "+dato;
        }
}