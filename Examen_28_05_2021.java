public class Examen_28_05_2021 {

    
    public static void main(String[] args) {

        
        ArbolBinario Arbolito = new ArbolBinario();
        Arbolito.agregarNodo(70, "F");
        Arbolito.agregarNodo(82, "R");
        Arbolito.agregarNodo(73, "I");
        Arbolito.agregarNodo(68, "D");
        Arbolito.agregarNodo(65, "A");
        Arbolito.agregarNodo(77, "M");
        Arbolito.agregarNodo(78, "N");
        Arbolito.agregarNodo(66, "B");
        Arbolito.agregarNodo(67, "C");
        
        

        System.out.println("PosOrden");
        if(!Arbolito.estaVacio()){
           Arbolito.posOrden(Arbolito.raiz);
        }        
    }
    
}
